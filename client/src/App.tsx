import React from "react";
import "./App.module.scss";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { BrowserRouter, Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" color="inherit" aria-label="menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="h6">News</Typography>
          </Toolbar>
        </AppBar>
      </header>
      <BrowserRouter>
        <Switch>
          <Route path="/about">
            <div />
          </Route>
          <Route path="/topics">
            <div />
          </Route>
          <Route path="/">
            <div />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
